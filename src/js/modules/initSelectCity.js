function initSelectCity() {
  "use strict";
  $(function () {
    function o(t) {
      t
        .children(".custom-select__body")
        .toggleClass("custom-select__body--hide"),
        t
          .find(".custom-select__arrow")
          .toggleClass("custom-select__arrow--active"),
        l(t);
    }
    function i(t, s) {
      $(".custom-select__options-container--hide").removeClass(
        "custom-select__options-container--hide"
      ),
        t.find(".custom-select__option[data-city-id]").each(function (t, e) {
          e = $(e);
          e.data("city-id") === s
            ? e.removeClass("custom-select__option--hide")
            : e.addClass("custom-select__option--hide");
        });
    }
    function a(t, e) {
      t.find(".custom-select__head-container").text(e);
    }
    function n(t) {
      t.siblings().removeClass("custom-select__option--active"),
        t.addClass("custom-select__option--active"),
        t.siblings().attr("data-city-id") &&
          (t.siblings().removeAttr("selected"), t.attr("selected", ""));
    }
    function d(t, e) {
      e
        ? t.addClass("custom-select--disabled")
        : t.removeClass("custom-select--disabled");
    }
    var t = $(".js-custom-selector"),
      l = function (t) {
        var e,
          s,
          c = t.find("[data-city-id][selected]");
        c.length &&
          ((e = (c = c.first()).data("city-id")),
          (s = t.find('[data-region-id="' + e + '"]')),
          i(t, e),
          a(t, c.text()),
          n(c),
          n(s),
          c.parent().scrollTop(c[0].offsetTop - 40));
      };
    t.each(function () {
      var t = $(this),
        e = t.find(".custom-select__head"),
        s = t.find(".custom-select__option[data-region-id]"),
        c = t.find(".custom-select__option[data-city-id]");
      e.click(function () {
        var e;
        o(t),
          (e = t),
          $(document).off("click.select"),
          $(document).on("click.select", function (t) {
            e.is(t.target) ||
              0 !== e.has(t.target).length ||
              (e
                .find(".custom-select__body")
                .hasClass("custom-select__body--hide") || o(e),
              $(this).off("click.select"));
          });
      }),
        s.click(function () {
          var t,
            e = $(this);
          (e = (t = e).data("region-id")),
            n(t),
            i(t.closest(".js-custom-selector"), e);
        }),
        c.click(function () {
          var t = $(this);
          !(function (t) {
            n(t);
            var e = t.closest(".js-custom-selector");
            a(e, t.text()), o(e);
          })(t),
            (function (t) {
              var e = t.closest(".js-custom-selector");
              d(e, !0);
              t = { id: t.data("id") };
              $.ajax({
                url: "/ajax/ajaxCityHandler.php",
                method: "POST",
                dataType: "json",
                data: t,
                success: function () {
                  d(e, !1);
                },
              });
            })(t);
        }),
        l(t);
    });
  });
}
