function initPlaceholder() {
  console.log("initPlaceholder");
  try {
    let inputForm = document.querySelectorAll(".Input__inp");
    const InputPlaceholder = document.querySelectorAll(".Input__placeholder");
    let numberСharacters = 0;
    let dataIndexInput;

    document.addEventListener("mousedown", function (event) {
      inputForm.forEach(function (itemInput) {
        InputPlaceholder.forEach(function (itemPlace) {
          numberСharacters = itemInput.value.length;
          dataIndexInput = itemInput.getAttribute("data-index-input");
          if (dataIndexInput === itemPlace.getAttribute("data-index-input")) {
            if (numberСharacters === 0) {
              itemPlace.classList.remove("Input__placeholder--star-active");
              itemPlace.classList.add("Input__placeholder--star");
            }
          }
        });
      });
    });

    inputForm.forEach(function (itemInput) {
      itemInput.addEventListener("click", function (event) {
        dataIndexInput = itemInput.getAttribute("data-index-input");
        InputPlaceholder.forEach(function (itemPlace) {
          if (dataIndexInput === itemPlace.getAttribute("data-index-input")) {
            itemPlace.classList.add("Input__placeholder--star-active");
            itemInput.oninput = function () {
              numberСharacters = itemInput.value.length;
              if (numberСharacters > 0) {
                itemPlace.classList.remove("Input__placeholder--star");
              } else {
                itemPlace.classList.add("Input__placeholder--star");
              }
            };
          }
        });
      });
    });

    InputPlaceholder.forEach(function (itemPlace) {
      itemPlace.addEventListener("click", function (event) {
        dataIndexInput = itemPlace.getAttribute("data-index-input");
        inputForm.forEach(function (itemInput) {
          if (dataIndexInput === itemInput.getAttribute("data-index-input")) {
            itemInput.focus();
            itemPlace.classList.add("Input__placeholder--star-active");
            itemInput.oninput = function () {
              numberСharacters = itemInput.value.length;
              if (numberСharacters > 0) {
                itemPlace.classList.remove("Input__placeholder--star");
              } else {
                itemPlace.classList.add("Input__placeholder--star");
              }
            };
          }
        });
      });
    });

    inputForm.forEach(function (itemInput) {
      InputPlaceholder.forEach(function (itemPlace) {
        document.addEventListener("mousedown", function (event) {
          if (event.target.className !== itemInput.classList.value) {
            if (numberСharacters === 0) {
              if (
                dataIndexInput === itemPlace.getAttribute("data-index-input")
              ) {
                itemPlace.classList.remove("Input__placeholder--star-active");
                itemPlace.classList.add("Input__placeholder--star");
              }
            } else {
              if (
                dataIndexInput === itemPlace.getAttribute("data-index-input")
              ) {
                itemPlace.classList.add("Input__placeholder--star-active");
                itemPlace.classList.remove("Input__placeholder--star");
              }
            }
          }
        });
      });
    });
  } catch {
    console.log("Ошибка initPlaceholder");
  }
}
