function initOpenClose() {
  console.log("initOpenClose");
  try {
    let getCode = document.getElementById("getcodesms");
    let modalWindow01 = document.getElementById("getcodeModal01");
    let modalWindow02 = document.getElementById("getcodeModal02");
    let modalWindow03 = document.getElementById("getcodeModal03");
    let comeWithPass = document.querySelectorAll("#comewithpass");
    let comeWidtSms = document.getElementById("comeWidtSms");
    let enterExit = document.getElementById("enterExit");

    window.addEventListener("click", (event) => {
      if (event.target.closest(".header__block-loginin")) {
        enterExit.addEventListener("click", () => {
          modalWindow01.style.visibility = "visible";
          modalWindow01.style.opacity = "1";
        });
        comeWithPass.forEach((item) => {
          item.addEventListener("click", () => {
            modalWindow01.style.visibility = "hidden";
            modalWindow01.style.opacity = "0";
            modalWindow02.style.visibility = "hidden";
            modalWindow02.style.opacity = "0";
            modalWindow03.style.visibility = "visible";
            modalWindow03.style.opacity = "1";
          });
        });

        comeWidtSms.addEventListener("click", () => {
          modalWindow01.style.visibility = "visible";
          modalWindow01.style.opacity = "1";
          modalWindow02.style.visibility = "hidden";
          modalWindow02.style.opacity = "0";
          modalWindow03.style.visibility = "hidden";
          modalWindow03.style.opacity = "0";
        });

        getCode.addEventListener("click", () => {
          modalWindow01.style.visibility = "hidden";
          modalWindow01.style.opacity = "0";
          modalWindow02.style.visibility = "visible";
          modalWindow02.style.opacity = "1";
        });
      } else {
        modalWindow01.style.visibility = "hidden";
        modalWindow01.style.opacity = "0";
        modalWindow02.style.visibility = "hidden";
        modalWindow02.style.opacity = "0";
        modalWindow03.style.visibility = "hidden";
        modalWindow03.style.opacity = "0";
      }
    });
  } catch (e) {
    console.log("Ошибка initOpenClose", e);
  }
}
