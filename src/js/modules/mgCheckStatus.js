function mgCheckStatus() {
  const statusName = document.querySelectorAll('[data-status-check]');
  const mgStatusBlock = document.querySelectorAll('.mg-status');
  try {
    console.log('mgCheckStatus');

    const drawGreen = (num) => {
      for (let j = 0; j <= num; j++) {
        mgStatusBlock[num]
          .querySelectorAll('.mg-check-status__block')
          [j].classList.add('mg-status-green');
      }
    };

    statusName.forEach((el, i) => {
      const status = el.getAttribute('data-status-check');

      switch (status) {
        case 'new':
          el.innerText = 'Новый';
          drawGreen(i);
          break;
        case 'accepted':
          el.innerText = 'Принят';
          drawGreen(i);
          break;
        case 'agreed':
          el.innerText = 'Согласован';
          drawGreen(i);
          break;
        case 'partially-shipped':
          el.innerText = 'Частично отгружен';
          drawGreen(i);
          break;
        case 'shipped':
          el.innerText = 'Отгружен';
          drawGreen(i);
          break;
        case 'completed':
          el.innerText = 'Выполнен';
          drawGreen(i);
          break;
        default:
          break;
      }
    });
  } catch (err) {
    console.log('Error mgCheckStatus', err);
  }
}
