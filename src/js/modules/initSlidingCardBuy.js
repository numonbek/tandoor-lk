function initSlidingCardBuy() {
  // console.log("initSlidingCardBuy");
  "use strict";

  $(function () {
    // $('.js-r-side-menu-trigger').on('click', function () {
    //     $('.n-base-content').addClass('n-base-content--hidden');
    //     $('.r-side-menu').addClass('r-side-menu--visible');
    //     document.body.style.overflow = 'hidden';
    // });

    // $('.js-m-menu-close').on('click', function () {
    //     $('.n-base-content').removeClass('n-base-content--hidden');
    //     $('.r-side-menu').removeClass('r-side-menu--visible');
    //     document.body.style.overflow = '';
    // });

    $(".js-dd-list-trigger").on("click", function () {
      $(this)
        .closest(".item-Card-buy__amount")
        .find(".item-Card-buy__list-detailed")
        .slideToggle(300);
    });

    // $('.js-catalog-root-link').on('click', function (e) {
    //     e.preventDefault();
    //     $(this).next('.js-dd-list-trigger').trigger('click');
    // });
  });
}
