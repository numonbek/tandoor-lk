function initCounter() {
  console.log("initCounter");

  try {
    window.addEventListener("click", (e) => {
      const input = e.target.closest(".Counter");
      if (!input) return;

      const inpControl = input.querySelector("input");
      const btnDecrement = e.target.closest("[data-decrement]");
      const btnIncrement = e.target.closest("[data-increment]");

      let dataValue, dataMin, dataMax, dataStep;

      dataValue = Number(inpControl.getAttribute("value"));
      dataMin = Number(inpControl.getAttribute("min"));
      dataMax = Number(inpControl.getAttribute("max"));
      dataStep = Number(inpControl.getAttribute("step"));

      // let baseValue = inpControl.value;
      // if (!isNaN(baseValue)) {
      // }
      const thirdTotal = {
        stock: 0,
        total: 0,
      };
      console.log("stock object", thirdTotal.stock);
      try {
        const parentPopup = e.target.closest(".popup-selection-master");
        var mainCounter = parentPopup.querySelector("[data-popup-counter]");

        var dataFirstCounter = document.querySelectorAll("[data-popup-first]");
        var dataTwoCounter = document.querySelectorAll("[data-popup-two]");
        var dataThirdCounter = document.querySelectorAll("[data-popup-third]");

        var firstChild = document.querySelectorAll("[data-first-child]");
        var twoChild = document.querySelectorAll("[data-two-child]");
        var thirdChild = document.querySelectorAll("[data-third-child]");
      } catch (err) {}

      // const ParentClosest = document.querySelector(
      //   ".popup-selection-master--four"
      // );

      // const first = ParentClosest.querySelector("[data-popup-counter]");

      // let second = ParentClosest.querySelector(
      //   ".popup-selection-master__item--active"
      // ).querySelector("[data-popup-counter]");
      // // let rrrr = (ghgh.innerText = "4967849874");
      // const ggg = middleClosest.closest(".popup-selection-master--four");
      // // let sdsdsd = (ggg.querySelector("[data-popup-counter]").innerText =
      //   "12346579");

      const PopupCounter = input.querySelector("[data-popup-counter]");
      let objectCounter = {};
      function funDec() {
        if (btnDecrement) {
          if (parseInt(inpControl.value) > dataMin) {
            // btnDecrement.removeAttribute("disabled");
            inpControl.value = parseInt(inpControl.value) - dataStep;
            inpControl.setAttribute("value", `${+inpControl.value}`);
            objectCounter = inpControl.value;
            PopupCounter.innerText = parseInt(1200) * parseInt(objectCounter);

            // console.log(
            //   "second",
            //   (activeCounter.innerText = +mainCounter.innerText)
            // );
            try {
              parentCounters();
              popupTotal();
              // dataFirstCounter.forEach((item, index) => {
              //   console.log("first", mainCounter);
              //   item.innerText = +mainCounter.innerText;
              // });
              // activeCounter.innerText = parseInt(inpControl.value);
              // activeCounter.innerText = +mainCounter.innerText;
            } catch (e) {}
          }
          // else {
          //   btnDecrement.setAttribute("disabled", "");
          // }
        }
      }
      function funInc() {
        if (btnIncrement) {
          if (parseInt(inpControl.value) < dataMax) {
            // btnIncrement.removeAttribute("disabled");

            inpControl.value = parseInt(inpControl.value) + dataStep;
            inpControl.setAttribute("value", `${+inpControl.value}`);
            PopupCounter.innerText = parseInt(PopupCounter.innerText) + 1;
            objectCounter = inpControl.value;
            PopupCounter.innerText = parseInt(1200) * parseInt(objectCounter);

            // console.log(
            //   "second",
            //   (activeCounter.innerText = +mainCounter.innerText)
            // );
            // inpControl.value = +inpControl.value + dataStep;
            try {
              parentCounters();
              popupTotal();
              // dataFirstCounter.forEach((item, index) => {
              //   console.log("first", mainCounter);
              //   item.innerText = +mainCounter.innerText;
              // });
              // activeCounter.innerText = parseInt(inpControl.value);
              // activeCounter.innerText = +mainCounter.innerText;
            } catch (e) {}
          }
          // else {

          //   btnIncrement.setAttribute("disabled", "");
          // }
        }
      }

      funDec();
      funInc();

      inpControl.oninput = function (event) {
        popupCounter();
        if (event.target.value < 1) {
          inpControl.value = dataMin;

          // console.log("1");
        } else {
          PopupCounter.innerText = 1200 * parseInt(event.target.value);

          if (inpControl.value < dataMin - 1) {
            inpControl.value = dataMin;

            inpControl.setAttribute("value", `${parseInt(event.target.value)}`);
          }

          if (inpControl.value > dataMax) {
            inpControl.value = dataMax;
            inpControl.setAttribute("value", `${parseInt(event.target.value)}`);
          }
          funDec();
          funInc();
          // console.log("1<");
        }
      };

      function parentCounters() {
        if (e.target.closest(".popup-selection-master--four")) {
          // firstChild.forEach(item, (index) => {
          //   item.innerText = +inpControl.value;
          // });
          dataFirstCounter.forEach((item, index) => {
            console.log("first", firstChild[index]);
            item.innerText = +mainCounter.innerText;
            firstChild[index].innerText = parseInt(inpControl.value);
          });
        }
        if (e.target.closest(".popup-selection-master--six")) {
          dataTwoCounter.forEach((item, index) => {
            console.log("first", mainCounter);
            item.innerText = +mainCounter.innerText;
            twoChild[index].innerText = parseInt(inpControl.value);
          });
        }
        if (e.target.closest(".popup-selection-master--eight")) {
          dataThirdCounter.forEach((item, index) => {
            console.log("first", mainCounter);
            item.innerText = +mainCounter.innerText;
            thirdChild[index].innerText = parseInt(inpControl.value);
          });
          // const windowTotal = querySelector(".popup-selection-master__list");
          // const dataFirstTotal = document.querySelector("[data-popup-first]");
          // const dataTwoTotal = document.querySelector("[data-popup-two]");
          // const dataThirdTotal = document.querySelector("[data-popup-third]");

          // const firstChildTotal = document.querySelector("[data-first-child]");
          // const twoChildTotal = document.querySelector("[data-two-child]");
          // const thirdChildTotal = document.querySelector("[data-third-child]");
          // console.log(
          //   "total",
          //   +firstChildTotal.innerText +
          //     +twoChildTotal.innerText +
          //     +thirdChildTotal.innerText
          // );
          // thirdTotal.stock +=
          //   +firstChildTotal.innerText +
          //   +twoChildTotal.innerText +
          //   +thirdChildTotal.innerText;
        }
      }
    });
  } catch {
    console.log("Ошибка initCounter");
  }
}
