function initMgBurger() {
  const navActive = document.querySelector(".nav-mg-active");
  const bugerMg = document.querySelector(".mg-navbar__burger");
  const bugerMgBurger = document.querySelector(".mg-navbar-wrapper");
  try {
    bugerMg.addEventListener("click", (event) => {
      console.log(bugerMg);
      bugerMgBurger.classList.toggle("nav-mg-active");
    });
    console.log("initMGBurger");
  } catch (err) {
    console.log("error initMgBurger");
  }
}
