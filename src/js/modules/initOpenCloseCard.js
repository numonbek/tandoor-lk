function initOpenCloseCard() {
  try {

		function removeDot(elemClass) {
			let mainActiveClass = '';
			let mainArray = elemClass.split("");
			let mainClass = mainArray.map((item, index)=>item == "." ? mainArray.splice(index, 0): mainActiveClass += item);
			return mainActiveClass;
		}

		function toggleShow(mainElem, elements, btn, animated = "slideToggle", addMainActive = false) {
			document.addEventListener("mousedown", (event) => {
				if (event.target.closest(mainElem) && event.target.closest(btn)) {
					let pathMain = event.target.closest(mainElem);
					let elemsToggleShow = [];

					if (addMainActive) {
						pathMain.classList.toggle(`${removeDot(mainElem)}--active`);
					}
					elements.forEach((itemElems) =>  {
						elemsToggleShow.push(pathMain.querySelector(`.${itemElems}`));
					});
					elemsToggleShow.forEach((itemElem) => {
						console.log();
						switch (animated) {
							case "toggle":
								$(itemElem).toggle(300);
								break;
							case "slideDown":
								$(itemElem).slideDown(300);
								break;
							case "slideUp":
								$(itemElem).slideUp(300);
								break;
							case "slideToggle":
								$(itemElem).slideToggle(300);
								break;
								case "fadeToggle":
									$(itemElem).fadeToggle(300);
									break;
						}
					});
				}
			});
		}
		
		toggleShow(".Card-product", ["Card-product__block-specification-js"], ".Card-product__btn-specifications-js", undefined, true);

		toggleShow(".Card-product", ["Card-product__block-have-info-js"], ".Card-product__have-icon-js", "slideToggle");
		toggleShow(".Card-product-aflat", ["Card-product-aflat__block-have-info-js"], ".Card-product-aflat__have-icon-js", "slideToggle");
		toggleShow(".Card-buy__wrapper-item-products", ["item-Card-buy__detailed"], ".item-Card-buy__amount-btn", "slideToggle");
		toggleShow(".ordering-page__block-delivery", ["ordering-page__delivery-map"], ".Button--js", "slideToggle", true);
		toggleShow(".information-account", ["information-account__content"], ".information-account__title", "slideToggle", true);



  } catch (e) {
    console.log("Ошибка initOpenCloseSpecifications" + e);
  }
}
