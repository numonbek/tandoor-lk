function initDropDown() {
  console.log("initDropDown");
  try {
		function hiddenDropDown() {
			const dropDownBtn = document.querySelectorAll(".dropdown__button")
			const dropDownList = document.querySelectorAll(".dropdown__list")
			dropDownBtn.forEach((item) => {
				item.classList.remove("dropdown__button--active");
			})
			dropDownList.forEach((item) => {
				item.classList.remove("dropdown__list--visible");
			})
		}

		function removeClasses() {
			const listItem = document.querySelectorAll(".dropdown__list-item");
			listItem.forEach((item) => {
				item.classList.remove("dropdown__list-item--active");
			})
		}

		document.addEventListener('click', (event) => { 
			if (event.target.closest(".dropdown")) {
				hiddenDropDown()
				const path = event.target.closest(".dropdown")
				if (event.target.closest(".dropdown__button")) {
					let dropDownBtn = path.querySelector(".dropdown__button")
					let dropDownList = path.querySelector(".dropdown__list")
					dropDownBtn.classList.toggle("dropdown__button--active")
					dropDownList.classList.toggle("dropdown__list--visible")
				}
	
				if (event.target.closest(".dropdown__list-item")) {
					let textBtn = path.querySelector(".dropdown__button-text")
					let pathList = event.target.closest(".dropdown__list-item")
					removeClasses()
					pathList.classList.add('dropdown__list-item--active')
					let listText = pathList.querySelector(".dropdown__list-item-text")
					textBtn.innerHTML = listText.textContent
					let imageBtn = path.querySelector(".dropdown__img")
					if (imageBtn) {
						let listImage = pathList.querySelector(".dropdown__img")
						imageBtn.setAttribute('src', listImage.getAttribute('src'))
					}
				}
			} else {
				hiddenDropDown()
			}
		})
  } catch {
    console.log("Ошибка initDropDown");
  }
}
