function initDragDrop() {
  console.log("initDragDrop");
  try {
    let dragfiles = document.querySelector(".dragfiles");
    let textmessages = $id("messages");

    function $id(id) {
      return document.getElementById(id);
    }

    // output information
    function Output(msg) {
      var m = $id("messages");
      m.textContent = msg;
    }

    Output("Вес файла не более 5МБ");

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
      Init();
    }

    // initialize
    function Init() {
      let fileselect = $id("fileselect"),
        filedrag = $id("filedrag"),
        submitbutton = $id("submitbutton");

      // file select
      fileselect.addEventListener("change", FileSelectHandler, false);

      // is XHR2 available?
      let xhr = new XMLHttpRequest(); // new XMLHttpRequest()
      if (xhr.upload) {
        // xhr.upload
        // file drop
        filedrag.addEventListener("dragover", FileDragHover, false);
        filedrag.addEventListener("dragleave", FileDragHover, false);
        filedrag.addEventListener("drop", FileSelectHandler, false);
        filedrag.style.display = "flex";
      }
    }

    function FileDragHover(e) {
      e.stopPropagation();
      e.preventDefault();
      if (e.type == "dragover") {
        if (!e.target.classList.contains("dragfiles--hover")) {
          dragfiles.className = dragfiles.className + " dragfiles--hover";
        }
      }
    }

    // file selection

    function FileSelectHandler(e) {
      // cancel event and hover styling
      FileDragHover(e);

      // fetch FileList object
      var files = e.target.files || e.dataTransfer.files;

      // process all File objects
      for (var i = 0, f; (f = files[i]); i++) {
        ParseFile(f);
      }
    }

    function ParseFile(file) {
      if (
        file.size <
        parseInt(fileselect.getAttribute("data-file-size")) * 1048576
      ) {
        Output("Название файла: " + file.name + "тип: " + file.type);
        textmessages.classList.remove(
          "dragfiles__text-warning-upload--warning"
        );
        textmessages.classList.add("dragfiles__text-warning-upload--success");
      } else {
        textmessages.classList.remove(
          "dragfiles__text-warning-upload--success"
        );
        textmessages.classList.add("dragfiles__text-warning-upload--warning");
        Output("Размер файла > 5Мб");
      }
    }
  } catch (e) {
    console.log(`Ошибка initDragDrop ${e}`);
  }
}
