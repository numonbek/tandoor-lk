function mgCheckSucess() {
  //const dataConNumber = document.querySelectorAll('[data-condition-number]');
  const orderItem = document.querySelectorAll('.ordering-page__list-item');
  try {
    console.log('mgCheckSuccess');
    try {
      const drawNumber = (closest, counter, num) => {
        const parentClass = document.querySelector(`${closest}`);
        const counterData = parentClass.querySelectorAll(`${counter}`);

        let initialNumber = num;
        counterData.forEach((item, index) => {
          if (!item.closest(`${closest}`)) return false;
          item.innerText = initialNumber;
          initialNumber++;
        });
      };
      drawNumber('.ordering-page__list', '[data-condition-number]', 1);
      drawNumber('.mg-navigation-list', '[data-condition-number]', 1);
    } catch (err) {}
    /* check fullfill */
    const checkInput = () => {
      // console.log('card', ggh.children.length > 0)
      orderItem.forEach((item, index) => {
        const thirdChild = document.querySelector('.delivery--select');
        const fourChild = document.querySelector('.payment--select');

        const firstBlock = orderItem[0].querySelector('.Card-buy__list-products');
        const secondBlock = orderItem[1].querySelector('form');
        const thirdBlock = orderItem[2].contains(thirdChild);
        const fourBlock = orderItem[3].contains(fourChild);
        if (firstBlock.closest('.checked')) {
          if (firstBlock.children.length > 0) {
            removeErrorAddSucess(0);
          } else {
            removeSucessAddError(0);
          }
        }
        if (secondBlock.closest('.checked')) {
          checkValidations(secondBlock);
          if (secondBlock.closest('.n-input-checked')) {
            removeErrorAddSucess(1);
          } else {
            removeSucessAddError(1);
          }
        }
        if (orderItem[2].closest('.checked')) {
          if (thirdBlock) {
            removeErrorAddSucess(2);
          } else {
            removeSucessAddError(2);
          }
        }
        if (orderItem[3].closest('.checked')) {
          if (fourBlock) {
            removeErrorAddSucess(3);
          } else {
            removeSucessAddError(3);
          }
        }
      });
    };

    /* add class checked*/
    const toggleClassOrder = (main, elem, classActive) => {
      document.addEventListener('click', (event) => {
        if (event.target.closest(main) && event.target.closest(elem)) {
          const mainPath = event.target.closest(main);
          const mainElement = event.target.closest(elem);
          const elements = mainPath.querySelectorAll(elem);

          elements.forEach((item) => {
            item.classList.remove(classActive);
          });
          mainElement.classList.add(classActive);
          mainElement.classList.add('checked');
        }
      });
    };

    toggleClassOrder(
      '.ordering-page__list',
      '.ordering-page__list-item',
      'ordering-page__list-item--select',
    );

    // const toggleClassAgent = (main, elem, classActive) => {
    //   document.addEventListener('click', (event) => {
    //     if (event.target.closest(main) && event.target.closest(elem)) {
    //       const mainPath = event.target.closest(main);
    //       const mainElement = event.target.closest(elem);
    //       const elements = mainPath.querySelectorAll(elem);
    //       const

    //       elements.forEach((item) => {
    //         item.classList.remove(classActive);
    //       });
    //       mainElement.classList.add(classActive);
    //       mainElement.classList.add('checked');
    //     }
    //   });
    // };

    toggleClassOrder(
      '.mg-navigation-list__container',
      '.ordering-page__list-title',
      'ordering-page__list-item--select',
    );

    /* hguy */
    window.addEventListener('click', (e) => {
      if (!e.target.closest('.ordering-page-checkstatus')) return false;

      if (e.target.closest('.ordering-page__list-item')) {
        const main = e.target.closest('.ordering-page__list-item');
        checkInput();
      } else {
        checkInput();
        orderItem.forEach((item) => {
          item.classList.remove('ordering-page__list-item--select');
        });
      }
    });

    /* clear functions */
    const clearClass = (element, className) => {
      element.forEach((item) => {
        item.classList.remove(className);
      });
    };
    // check function for form
    /**
     * @param {checkValidations} content get form
     * @param {checkValidations} inputs get all inputs from content
     */
    function checkValidations(content) {
      const inputs = content.querySelectorAll('input');
      const first = inputs[0].closest('.input-validation--valid');
      const second = inputs[1].closest('.input-validation--valid');
      const third = inputs[2].closest('.input-validation--valid');
      inputs.forEach((item, index) => {
        // console.log("item", item.contains(fdfd))
        if (first && second && third) {
          item.closest('form').classList.add('n-input-checked');
        } else {
          item.closest('form').classList.remove('n-input-checked');
        }
      });
    }

    function removeSucessAddError(num) {
      orderItem[num].classList.remove('ordering-page__list-item--disable');
      orderItem[num].classList.remove('ordering-page__list-item--success');
      orderItem[num].classList.add('ordering-page__list-item--error');
    }

    function removeErrorAddSucess(num) {
      orderItem[num].classList.remove('ordering-page__list-item--disable');
      orderItem[num].classList.remove('ordering-page__list-item--error');
      orderItem[num].classList.add('ordering-page__list-item--success');
    }
  } catch (err) {
    console.log('Error mgCheckSuccess', err);
  }
}
