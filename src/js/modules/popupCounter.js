function popupCounter(firstnum, secondnum) {
  try {
    console.log("popupCounter");
    const classicTotal = document.querySelector(".pick-up-classic");
    const moldingTotal = document.querySelector(".pick-up-molding");

    const objectCounter = Object.create({
      countClassic: firstnum ? firstnum : 0,
      countMolding: secondnum ? secondnum : 0,
    });

    moldingTotal
      .querySelectorAll(".popup-interior-door__item--active")
      .forEach((item, index) => {
        objectCounter.countMolding =
          +objectCounter.countMolding +
          +item.querySelector("[data-popup-counter]").innerText;
      });
    classicTotal
      .querySelectorAll(".popup-interior-door__item--active")
      .forEach((item, index) => {
        objectCounter.countClassic =
          +objectCounter.countClassic +
          +item.querySelector("[data-popup-counter]").innerText;
      });

    let total = document.querySelectorAll("[data-popup-total]");
    total.forEach((item, index) => {
      item.innerText =
        objectCounter.countClassic + objectCounter.countMolding || 0;
    });
  } catch (e) {
    console.log("Error popupCounter");
  }
}
